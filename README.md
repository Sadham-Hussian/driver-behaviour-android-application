# IoT application for Android
IoT application for interacting with IBM Watson IoT Platform.
The application turns your mobile device into a sensor that publishes and receives data to and from the cloud using the MQTT protocol.

## Short Description
Watson IoT Platform is a cloud-hosted service to simplify managing all of your IoT devices. The key features provided by the service include:
- Device management
- Scalable connectivity
- Secure communication
- Historical data

This application demonstrates using an Android device as one an IoT device, and provides a variety of events and commands that it can publish or receive data to and from.

Events and commands are user defined values used to differentiate the data that you publish or receive. For example, if you have a device that is publishing GPS coordinates, you may choose to publish it as a 'GPS' event. Or, if you want to send an alert command to a device, you may choose to publish it as an 'alert' or 'notification' command.

The application can publish data to the following event topics:
- Accelerometer (accel event)
- Touchmove (touchmove event)
- Text (text event)

The application can receive data on the following command topics:
- Color (color command)
- Light (light command)
- Text (text command)
- Alert (alert command)

## How it works
A device that is registered with Watson IoT Platform may publish and subscribe to data that is presented as either an event or command using the MQTT protocol.
The Eclipse Paho MQTT Android Service is used to publish and subscribe to Watson IoT Platform. This can be downloaded from
[Eclipse Paho MQTT Android Service](http://www.eclipse.org/paho/clients/android/).

MQTT is a lightweight messaging protocol that supports publish/subscribe messaging. With MQTT, an application publishes messages to a topic. These messages may then be received by another application that is subscribed to that topic. This allows for a detached messaging network where the subscribers and publishers do not need to be aware of each other.
The topics used by this application can be seen in the table below:

## Topics
|Topic|Sample Topic|Sample Messages|
|:---------- |:---------- |:------------|
|`iot-2/evt/<eventId>/fmt/json`|`iot-2/evt/touchmove/fmt/json`|`{"d":{"screenX":0,"screenY":0,"deltaX":0,"deltaY":0}}`|
|`iot-2/cmd/<commandId>/fmt/json`|`iot-2/cmd/light/fmt/json`|`{"d":{"light":"toggle"}}`<br/>`{"d":{"light":"on"}}`<br/>`{"d":{"light":"off"}}`|

